<?php

/**
  Coded by: Sandip Debnath
  Email: sandip5004@gmail.com
  Site: gruponovolux.com
**/


error_reporting(E_ERROR | E_WARNING | E_PARSE);
ob_implicit_flush(true);			//show output during execution in browsers
set_time_limit(E_ALL);					//the script will run for unlimited time
ini_set('memory_limit', '900M');	//maximum memory allocation is 900 MB
header('Content-Type: text/html; charset=utf-8');

include_once(dirname(__FILE__) . '/includes/fn-webscrape.php');
include_once(dirname(__FILE__) . '/includes/config-ws.php');
@unlink(COOKIES);
@unlink(BROWSED_LINKS_TO_FILE);



/** TESTS **/
function test() {
	
    /*$url = 'http://media.juinsa.es/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/mirror/complementarias/60598/60598-01.jpg';
    $img = 'D:\USUARIOS\Oscar\EMPRESA\OZERTIS\SISTEMAS\Desarrollo\60598-01.jpg';
    file_put_contents($img, file_get_contents($url));
    $log = new Logging();
    $log->lfile(LOG_FILE);
    $log->lwecho('ASDF');
	$string = 'CL&Aacute;SICO RESIDENCIAL';
	echo '0-'.html_entity_decode($string).PHP_EOL;
	echo '1-'.htmlspecialchars_decode($string, ENT_NOQUOTES).PHP_EOL;
	echo '2-'.htmlspecialchars($string, ENT_COMPAT,'ISO-8859-1', true).PHP_EOL;
	echo '3-'.htmlentities($string, ENT_COMPAT,'ISO-8859-1', true).PHP_EOL;
	echo '4-'.htmlentities($string).PHP_EOL;*/
	
	$original_image_path = "./images/1019-12ab_1.jpg";
	$size = getimagesize($original_image_path);   print_r($size);

	$resized_image_path = "./images/1019-12ab_1_resized1.jpg";
	resize_image1($original_image_path, $resized_image_path, 600, 600, 75);		// FUNCIONA
	
	$resized_image_path = "./images/1019-12ab_1_resized2.jpg";
	$params = array( 'constraint' => array('width'=>600, 'height'=>600, 'quality'=>75) );
	resize_image2($original_image_path, $resized_image_path, $params);          // NO FUNCIONA BIEN, si es más pequeña, agranda la imagen

	$resized_image_path = "./images/1019-12ab_1_resized3.jpg";
	resize_image3($original_image_path, $resized_image_path, 600, 600);         // NO FUNCIONA BIEN, si es más pequeña, no copia la imagen

}
//test(); exit;








/** EXECUTION **/

$productCounter=0;
// Command line arguemnt
startCrawling($argv);






/** MAIN **/

function startCrawling($argv){
	
    if (@$argv[1] == 'ledsc4'){
		
		
		
		
		
		
		
		
		
	
    } elseif (@$argv[1] == 'mimax'){
        @unlink(MIMAX_CSV);
	
        mmxNewCSV(MIMAX_CSV);
	
        mmxStartCrawling();
        @unlink(COOKIES);
		
    } elseif (@$argv[1] == 'gnovolux') {
		
        if (@$argv[2] == '') {
          
			echo "Error, especifique si precios o productos".PHP_EOL;
			
        } elseif (@$argv[2] == 'precios') {
        
			@unlink(GNX_CSV_IMPORT_LOCAL);
			@unlink(GNX_CSV_DATA);

			gnxNewImportCsv(GNX_CSV_IMPORT_LOCAL);
			gnxNewWebDataCsv(GNX_CSV_DATA);

			gnxWebLogin();
			
			//$log->lecho('Copiando archivo de datos de local "'.$localWebImportFile.'" a remoto "'.$remoteWebImportFile);
			ftpUploadFile ( ENCHUFIX_SERVER, ENCHUFIX_FTP_USER, ENCHUFIX_FTP_PASS, GNX_CSV_IMPORT_LOCAL, GNX_CSV_IMPORT_REMOTE, false, $debug );
			
        } elseif (@$argv[2] == 'productos') {
			
			echo "Módulo en construcción".PHP_EOL;
			
			
			
			
			
			
			
			
			
        }
        //@unlink(BROWSED_LINKS_TO_FILE);
        @unlink(COOKIES);
        
    } elseif (@$argv[1] == 'unix') {
		
        if (@$argv[2] == '') {
            @unlink(UNIX_LOC_XML_IX);
            unixWebLogin('ixia');
            @unlink(UNIX_LOC_XML_JU);
            unixWebLogin('juinsa');
            @unlink(UNIX_LOC_XML_UN);
            unixWebLogin('unimasa');
			
        } elseif (@$argv[2] == 'ixia') {
            @unlink(UNIX_LOC_XML_IX);
            unixWebLogin('ixia');
			
        } elseif (@$argv[2] == 'juinsa') {
            @unlink(UNIX_LOC_XML_JU);
            unixWebLogin('juinsa');
			
        } elseif (@$argv[2] == 'unimasa') {
            @unlink(UNIX_LOC_XML_UN);
            unixWebLogin('unimasa');
        }
        //@unlink(BROWSED_LINKS_TO_FILE);
        @unlink(COOKIES);
    }
}







/** UNIX **/

function unixWebLogin($what){
    $log = new Logging();
    $log->lfile(LOG_FILE);
    global $productCounter;
    $productCounter=0;
    if ($what=='ixia') {
        $url = 'http://www.ixia.es/customer/account/login/';
        $loginurl = 'http://www.ixia.es/customer/account/loginPost/';
        $confirmurl='http://www.ixia.es/customer/account/';
        $origin='http://www.ixia.es';
        $host='www.ixia.es';
    }
    elseif ($what=='unimasa') {
        $url = 'http://www.unimasa.es/customer/account/login/';
        $loginurl = 'http://www.unimasa.es/customer/account/loginPost/';
        $confirmurl='http://www.unimasa.es/customer/account/';
        $origin='http://www.unimasa.es';
        $host='www.unimasa.es';
    }
    elseif ($what=='juinsa'){
        $url = 'http://www.juinsa.es/customer/account/login/';
        $loginurl='http://www.juinsa.es/customer/account/loginPost/';
        $confirmurl='http://www.juinsa.es/customer/account/';
        $origin='http://www.juinsa.es';
        $host='www.juinsa.es';
    }
    $ch = curl_init();
    curlDownload($ch, $url);
    sleep(rand(1, 3));
    try {
        $data = array(                                             //form post data or authentication details
            "login[username]" => UNIX_USER,
            "login[password]" => UNIX_PASS,
            "send" => ''
        );
        $data_string = http_build_query($data);
        curl_setopt($ch, CURLOPT_URL, $loginurl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                // this is the header sent along with the authentication details
            'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding:gzip, deflate',
            'Accept-Language:en-US,en;q=0.8',
            'Cache-Control:max-age=0',
            'Connection:keep-alive',
            'Content-Length:' . strlen($data_string),
            'Content-Type:application/x-www-form-urlencoded',
            'Host:'.$host,
            'Origin:'.$origin,
            'Referer:'.$url,
            'Upgrade-Insecure-Requests:1',
        ));
        curl_setopt($ch, CURLOPT_POST, true);                      // tell curl you want to post something
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);        // define what you want to post
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);            // return the output in string format
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, BROWSER);
        curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
        curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIES);
        curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIES);
        curl_exec($ch); // execute
        $content = curlDownload($ch, $confirmurl);

        if (stristr($content, 'account/logout/') && stristr($content,'Welcome')) {
            $log->lwecho("Login Success".' ('.$what.')');
            try {
                //Parsing and saving in xml will be done here
                unixTraverseThroughLeftMenus($content,$ch,$what);
                //gnxVisitPagesAndGrabData($url='http://customer.gruponovolux.com/category.php?n=50&id_category=3&p=1',$ch);
            } catch (Exception $e) {
                $log->lwecho('Caught exception while parsing the site: ', $e->getMessage());
            }
        } else {
            $log->lwecho("Login Not Successful".' ('.$what.')');
            exit();
        }
    } catch (Exception $e) {
        $log->lwecho('Caught exception while login: ', $e->getMessage(), "\n");
    }
    //curl_close($ch);
}

function unixTraverseThroughLeftMenus($content,$ch,$type){
	$log = new Logging();
	$log->lfile(LOG_FILE);
    try{
        if (!is_dir(UNIX_LOC_IMG_ORG1))
            mkdir(UNIX_LOC_IMG_ORG1,0777);
        if ($type=='ixia')
            unixCreateXML(UNIX_LOC_XML_IX);
        elseif ($type=='juinsa')
            unixCreateXML(UNIX_LOC_XML_JU);
        elseif ($type=='unimasa')
            unixCreateXML(UNIX_LOC_XML_UN);

        $pattern = '/<ul\s*id=[\"\']nav[\"\'].*>(.*)<\/ul>\s*<\/div>/Usi';
        preg_match($pattern, $content, $result);

        $cont = $result[1];

        $pattern = '/<li\s*class=[\"\']level1.*>\s*<a.*href=[\"\'](.*)[\"\']/Usi';
        preg_match_all($pattern, $cont, $result);

        $urlarr=$result[1];
        if (count($urlarr)>0){
            foreach($urlarr as $menuurl){
                if (stristr($menuurl,UNIX_TEST_CATEGORY)) 
                    unixVisitPagesAndGrabData(trim($menuurl),$ch,$type);
            }
        }
    }
    catch(Exception $e){
        $log->lwecho('Could Not Find Any anchor point in the left menus: ', $e->getMessage());
    }
}
function unixVisitPagesAndGrabData($url,$ch,$type){
	$log = new Logging();
	$log->lfile(LOG_FILE);
    try{
        //$log->lwecho($url);
        sleep(rand(0,5));
        $content=curlDownload($ch,$url);

        $pattern = '/http.*\/\/.*\/(.*)\/(.*)$/Usi';
        preg_match($pattern, $url, $result);
        $log->lwecho("Now Scraping Category: ".strtoupper($result[1]).' > '.strtoupper($result[2]));

        $pattern = '/<li\s*class=[\"\']item.*>(.*)<\/li>/Usi';
        preg_match_all($pattern, $content, $result);
        if (count($result[1])>0){
            foreach($result[1] as $productdetails){
                unixScrapeDetails($productdetails,$ch,$type);
            }
        }
        flush();
        $nextpage=unixNextPageNumber($content);

        if ($nextpage)
            unixVisitPagesAndGrabData($nextpage, $ch,$type);
    }
    catch (Exception $e) {
        $log->lwecho('Could Not Find Any anchor point in All products: ', $e->getMessage());
    }
}
function unixScrapeDetails($content,$ch,$type){
	$log = new Logging();
	$log->lfile(LOG_FILE);
    try {
        $price = unixProductPrice($content);
        if ($price >= UNIX_PRICE_THRESHOLD){
            $url = unixProductUrl($content);

            if (urlNotBrowsed($url))
                unixGrabDataFromUrl($url,$ch,$type);

        } else {
            //$log->lwecho($price . ' Lower than Threshold ');
        }
        flush();
        ob_flush();
    }
    catch (Exception $e) {
        $log->lwecho('Could Not Find Any anchor point inside single product: ', $e->getMessage());
    }
}
function unixGrabDataFromUrl($url,$ch,$type){
	$log = new Logging();
	$log->lfile(LOG_FILE);
    global $productCounter;
    try {
        $content = curlDownload($ch,$url);
        $minqty = unixMinqty($content);
        $breadcumb = unixBreadcumb($content);
        $prodtype = $breadcumb['type'];
        $prodcat = $breadcumb['category'];
        $image = unixImage($content, UNIX_RESIZE_IMAGES);
        $name = unixName($content);
        $description = unixDescription($content);
        $price = unixPrice($content);
        $ref = unixRef($content);
        if ($type=='ixia')      $stock = unixAvailableUnits1($content);
        else                    $stock = unixAvailableUnits2($content);
        $stock = (trim($stock)!='')?$stock:'0';
		if ($stock=='Si') $stock = 100;
		if ($stock=='No') $stock = 0;
        $lengthwidth = unixLengthwidth($content);
        $length = (trim($lengthwidth['length'])!='' && is_numeric($lengthwidth['length']) && 0<$lengthwidth['length']) ? $lengthwidth['length']: '0';
        $width = (trim($lengthwidth['width'])!='' && is_numeric($lengthwidth['width']) && 0<$lengthwidth['width']) ? $lengthwidth['width']: '0';
        $height = (trim($lengthwidth['height'])!='' && is_numeric($lengthwidth['height']) && 0<$lengthwidth['height']) ? $lengthwidth['height']: '0';
        $unit = (trim($lengthwidth['height'])!='') ? $lengthwidth['unit'] : '.';

        $color = pretreat(unixColor($content));
        $color = (trim($color)!='') ? $color: '.';
        $collection = pretreat(unixCollection($content));
        $collection = (trim($collection)!='') ? $collection: '.';
        $pattern = pretreat(unixPattern($content));
        $pattern = (trim($pattern)!='') ? $pattern: '.';
        $material = pretreat(unixMaterial($content));
        $material = (trim($material)!='') ? $material: '.';
        $piece = pretreat(unixPiece($content));
        $piece = (trim($piece)!='') ? $piece: '.';
        $season = pretreat(unixSeason($content));
        $season = (trim($season)!='') ? $season: '.';
        $model = pretreat(unixModel($content));
        $model = (trim($model)!='') ? $model: '.';
        $multipack = pretreat(unixMultipack($content));
        $multipack = (trim($multipack)!='') ? $multipack: '.';
        $assortment = pretreat(unixAssortment($content));
        $assortment = (trim($assortment)!='') ? $assortment: '.';
        $use = pretreat(unixUse($content));
        $use = (trim($use)!='') ? $use: '.';
        $fetchdisp = unixFetchdisp($content);
        $fetchdisp = (is_numeric($stock) && $stock>0) ? "0000-00-00": $fetchdisp;
        $fetchdisp = (trim($stock)=='') ? "." : $fetchdisp;
        $moreimages = unixMoreimages($content, UNIX_RESIZE_IMAGES);

        $fetchdisp = convertMonthToDate($fetchdisp);
		$availability = (is_numeric($stock) && $stock>0)?'in stock':'out of stock';

        $price=str_ireplace('.',',',$price);

        $array = array(
            "url"=>$url,
            "Category" => $prodcat,
            "minqty"=>$minqty,
            "type"=>$prodtype,
            "Image" => $image,
            "name" => $name,
            "description" => $description,
            "price" => $price,
            "ref" => $ref,
            "stock" => $stock,
            "availability" => $availability,
            "multipack" => $multipack,
            "length" => $length,
            "width" => $width,
            "height" => $height,
            "unit" => $unit,
            "color" => $color,
            "collection" => $collection,
            "pattern" => $pattern,
            "material" => $material,
            "piece" => $piece,
            "season" => $season,
            "model" => $model,
            "fetchdisp" => $fetchdisp
        );

        //print_r($array); 

        $item="<item>
        <ID>$ref</ID>
        <TITLE>$name</TITLE>
        <DESCRIPTION>$description</DESCRIPTION>
        <AVAILABILITY>$availability</AVAILABILITY>
        <STOCK>$stock</STOCK>
        <PRICE>$price</PRICE>
        <MIN_QTY>$minqty</MIN_QTY>
        <STD_QTY>0</STD_QTY>
        <UNIT_WEIGHT>0</UNIT_WEIGHT>
        <UNIT_WEIGHT_UNIT>KG</UNIT_WEIGHT_UNIT>
        <PRODUCT_CATEGORY>$prodcat</PRODUCT_CATEGORY>
        <PRODUCT_TYPE>$prodtype</PRODUCT_TYPE>
        <GTIN>0</GTIN>
        <COLOR>$color</COLOR>
        <COLLECTION>$collection</COLLECTION>
        <PATTERN>$pattern</PATTERN>
        <MATERIAL>$material</MATERIAL>
        <MODEL>$model</MODEL>
        <PIECE>$piece</PIECE>
        <MULTIPACK>$multipack</MULTIPACK>
        <ASSORTMENT>$assortment</ASSORTMENT>
        <USE>$use</USE>
        <SEASON>$season</SEASON>
        <LENGTH>$length</LENGTH>
        <WIDTH>$width</WIDTH>
        <HEIGHT>$height</HEIGHT>
        <UNIT_LWH>CM</UNIT_LWH>
        <FECHDISP>$fetchdisp</FECHDISP>
        <IMAGE_LINK>$image</IMAGE_LINK>
        <STD_LENGTH>0</STD_LENGTH>
        <STD_WIDTH>0</STD_WIDTH>
        <STD_HEIGHT>0</STD_HEIGHT>
        <STD_LWH>CM</STD_LWH>
        $moreimages
    </item>";
        $log->lwecho("Scraping SKU: $ref ($stock)   $name   Total Scraped Products: ".++$productCounter);
        if ($type=='ixia')
            unixInsertinXML(UNIX_LOC_XML_IX,$item);
        elseif ($type=='juinsa')
            unixInsertinXML(UNIX_LOC_XML_JU,$item);
        elseif ($type=='unimasa')
            unixInsertinXML(UNIX_LOC_XML_UN,$item);

        saveBrowsedUrl($url);
        sleep(rand(1,5));
    }
    catch (Exception $e) {
        $log->lwecho('Could Not enter product page: ', $e->getMessage());
    }
}










/** GNX **/

function gnxWebLogin(){
    $url = 'http://customer.gruponovolux.com/authentication.php';

    $ch = curl_init();
    curlDownload($ch, $url);
    sleep(rand(1, 3));
    try {

        $data = array( //form post data or authentication details
            "email" => GNX_USER,
            "passwd" => GNX_PASS,
            "SubmitLogin" => 'Identificación'
        );
        $data_string = http_build_query($data);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( // this is the header sent along with the authentication details
            'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding:gzip, deflate',
            'Accept-Language:en-US,en;q=0.8',
            'Cache-Control:max-age=0',
            'Connection:keep-alive',
            'Content-Length:' . strlen($data_string),
            'Content-Type:application/x-www-form-urlencoded',
            'Host:customer.gruponovolux.com',
            'Origin:http://customer.gruponovolux.com',
            'Referer:http://customer.gruponovolux.com/authentication.php',
            'Upgrade-Insecure-Requests:1',
        ));
        curl_setopt($ch, CURLOPT_POST, true);  // tell curl you want to post something
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); // define what you want to post
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, BROWSER);
        curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
        curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIES);
        curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIES);
        curl_exec($ch); // execute
        $content = curlDownload($ch, $Url = 'http://customer.gruponovolux.com/my-account.php');

        if (stristr($content, '?mylogout')) {
            echo "Login Success";
            try {
                //Parsing and saving in csvs will be done here
                gnxTraverseThroughLeftMenus($content,$ch);
                //gnxVisitPagesAndGrabData($url='http://customer.gruponovolux.com/category.php?n=50&id_category=3&p=1',$ch);
            } catch (Exception $e) {
                echo 'Caught exception while parsing the site: ', $e->getMessage();
            }
        } else {
            echo "Login Not Successful".PHP_EOL;
            exit();
        }
    } catch (Exception $e) {
        echo 'Caught exception while login: ', $e->getMessage(), "\n";
    }
    //curl_close($ch);
}
function gnxTraverseThroughLeftMenus($content,$ch){
    try{
        $pattern = '/id=[\"\']left_column[\"\'].*<ul.*>(.*)<\/ul>/Usi';
        preg_match($pattern, $content, $result);

        $cont = $result[1];

        $pattern = '/<a.*href=[\'\"](.*)[\'\"]/Usi';
        preg_match_all($pattern, $cont, $result);

        if (count($result[1])>0){
            foreach($result[1] as $menuurl){
                gnxVisitPagesAndGrabData($menuurl.'&n=50&p=1',$ch);
            }
        }
    }
    catch(Exception $e){
        echo 'Could Not Find Any anchor point in the left menus: ', $e->getMessage();
    }
}
function gnxVisitPagesAndGrabData($url,$ch){
    try {
        //echo $url.PHP_EOL;
        sleep(rand(0,5));
        $content=curlDownload($ch,$url);
        $pattern = '/<ul\s*id=[\"\']product_list[\"\'].*>(.*)<\/ul>/Usi';
        preg_match($pattern, $content, $result);

        $cont = $result[1];

        $pattern = '/id=[\"\']lg\-cabecera\-secciones[\"\'].*>.*<p>(.*)<.*p.*>/Usi';
        preg_match($pattern, $content, $result);
        $cat = trim($result[1]);

        echo "Now Scraping Category: ".strtoupper($cat).PHP_EOL;

        $pattern = '/<li.*>(.*)<\/li>/Usi';
        preg_match_all($pattern, $cont, $result);

        if (count($result[1])>0){
            foreach($result[1] as $productdetails){
                gnxScrapeDetails($productdetails,$cat);
            }
        }
        $nextpage=gnxNextPageNumber($content);
        flush();
        if ($nextpage){
            $nextpageurl=preg_replace('/\&p=.*$/Usi','&p='.$nextpage,$url);
            gnxVisitPagesAndGrabData($nextpageurl, $ch,$cat);
        }
    }
    catch (Exception $e) {
        echo 'Could Not Find Any anchor point in the left menus: ', $e->getMessage();
    }
}
function gnxScrapeDetails($content,$cat){
    global $productCounter;
    global $filterarr;
	
	$_cat = $cat;
	$_id = gnxSku($content);
    $_name = gnxName($content);
    $_price = gnxPrice($content);
	$_availavility = gnxStatus($content);
	$_isInStock = gnxStatus($content);
	$_envio = GNX_DAYS_SHIPPING;
	$_customStockStatus = ($_isInStock)?"Disponible":"Consultar";
	foreach($filterarr as $catarr){
        if ( $cat==$catarr['cat'] && $_isInStock==0){
			$_isInStock = 1;
			$_envio=$catarr['days'];
			$_customStockStatus = $catarr['stock-status'];
            break;
        }
    }
	
	$websites = 'enchufix';
	$sku = 'gnx-'.strtolower($_id);
	$status = 1;
	$qty = 100;			//($_isInStock)?100:0;
	$isInStock = 1;		//$_isInStock;
	$customStockStatus = $_customStockStatus;
	$envio = ($_isInStock)?$_envio:"";
	$badge = ($_isInStock)?"garantia,envio,devolucion":"garantia,devolucion";
    $pvr = str_ireplace(',', '.', $_price);
		$cost = $pvr - $pvr * GNS_PVR_DISCOUNT;
		$pprice = $cost + $cost * GNX_PRICE_MARGIN;
    $cost=str_ireplace('.',',',$cost);
    $pprice=str_ireplace('.',',',$pprice);
	$specialprice='';

    echo "Scraping SKU: $_cat  $_id  $_name     Total Scraped Products: ".++$productCounter.PHP_EOL;
	
    if (trim($sku) != '' || trim($name) != '') {
		// $list[] = array('category','id','name','availavility','is_in_stock','envio','price');
		saveArrayInCsv(GNX_CSV_DATA, array($_cat, $_id, $_name, $_availavility, $_isInStock, $_envio, $_price));
        if (GNX_WITH_PRICES)
			// $list[] = array('websites','sku','status','qty','is_in_stock','custom_stock_status','envio','badge','cost','price','special_price');
			saveArrayInCsv(GNX_CSV_IMPORT_LOCAL, array('enchufix', $sku, $status, $qty, $isInStock, $customStockStatus, $envio, $badge, $cost, $pprice, $specialprice));
        else
			// $list[] = array('websites','sku','status','qty','is_in_stock','custom_stock_status','envio','badge');
			saveArrayInCsv(GNX_CSV_IMPORT_LOCAL, array('enchufix', $sku, $status, $qty, $isInStock, $customStockStatus, $envio, $badge));
        //$this->saveBrowsedUrl($url);
    }
    flush();
    ob_flush();
}


?>